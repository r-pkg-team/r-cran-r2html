#!/bin/sh

ZARCHIVE=activewidgets-grid-1.0.0-free.zip
wget -N https://downloads.sourceforge.net/project/activeui/Grid%20widget/1.0/"$ZARCHIVE"
unzip "$ZARCHIVE"
mv ActiveWidgets/source/lib .
rm -rf ActiveWidgets "$ZARCHIVE"
tar -caf grid.js.tar.xz lib
rm -rf lib
